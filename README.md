# qcrawler

Lightning Fast and Elegant Scraping Framework for Gophers

qcrawler provides a clean interface to write any kind of crawler/scraper/spider.

With qcrawler you can easily extract structured data from websites, which can be used for a wide range of applications, like data mining, data processing or archiving.

[![GoDoc](https://godoc.org/gitlab.com/chedched/qcrawler?status.svg)](https://pkg.go.dev/gitlab.com/chedched/qcrawler/v2)
[![Backers on Open Collective](https://opencollective.com/qcrawler/backers/badge.svg)](#backers) [![Sponsors on Open Collective](https://opencollective.com/qcrawler/sponsors/badge.svg)](#sponsors) [![build status](https://gitlab.com/chedched/qcrawler/actions/workflows/ci.yml/badge.svg)](https://gitlab.com/chedched/qcrawler/actions/workflows/ci.yml)
[![report card](https://img.shields.io/badge/report%20card-a%2B-ff3333.svg?style=flat-square)](http://goreportcard.com/report/goqcrawler/qcrawler)
[![view examples](https://img.shields.io/badge/learn%20by-examples-0077b3.svg?style=flat-square)](https://gitlab.com/chedched/qcrawler/tree/master/_examples)
[![Code Coverage](https://img.shields.io/codecov/c/github/goqcrawler/qcrawler/master.svg)](https://codecov.io/github/goqcrawler/qcrawler?branch=master)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fgoqcrawler%2Fqcrawler.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Fgoqcrawler%2Fqcrawler?ref=badge_shield)
[![Twitter URL](https://img.shields.io/badge/twitter-follow-green.svg)](https://twitter.com/goqcrawler)

## Features

- Clean API
- Fast (>1k request/sec on a single core)
- Manages request delays and maximum concurrency per domain
- Automatic cookie and session handling
- Sync/async/parallel scraping
- Caching
- Automatic encoding of non-unicode responses
- Robots.txt support
- Distributed scraping
- Configuration via environment variables
- Extensions

## Example

```go
func main() {
	c := qcrawler.NewCollector()

	// Find and visit all links
	c.OnHTML("a[href]", func(e *qcrawler.HTMLElement) {
		e.Request.Visit(e.Attr("href"))
	})

	c.OnRequest(func(r *qcrawler.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.Visit("http://go-qcrawler.org/")
}
```

See [examples folder](https://gitlab.com/chedched/qcrawler/tree/master/_examples) for more detailed examples.

## Installation

Add qcrawler to your `go.mod` file:

```
module github.com/x/y

go 1.14

require (
        gitlab.com/chedched/qcrawler/v2 latest
)
```

## Bugs

Bugs or suggestions? Visit the [issue tracker](https://gitlab.com/chedched/qcrawler/issues) or join `#qcrawler` on freenode

## Other Projects Using qcrawler

Below is a list of public, open source projects that use qcrawler:

- [greenpeace/check-my-pages](https://github.com/greenpeace/check-my-pages) Scraping script to test the Spanish Greenpeace web archive.
- [altsab/gowap](https://github.com/altsab/gowap) Wappalyzer implementation in Go.
- [jesuiscamille/goquotes](https://github.com/jesuiscamille/goquotes) A quotes scrapper, making your day a little better!
- [jivesearch/jivesearch](https://github.com/jivesearch/jivesearch) A search engine that doesn't track you.
- [Leagify/qcrawler-draft-prospects](https://github.com/Leagify/qcrawler-draft-prospects) A scraper for future NFL Draft prospects.
- [lucasepe/go-ps4](https://github.com/lucasepe/go-ps4) Search playstation store for your favorite PS4 games using the command line.
- [yringler/inside-chassidus-scraper](https://github.com/yringler/inside-chassidus-scraper) Scrapes Rabbi Paltiel's web site for lesson metadata.
- [gamedb/gamedb](https://github.com/gamedb/gamedb) A database of Steam games.
- [lawzava/scrape](https://github.com/lawzava/scrape) CLI for email scraping from any website.
- [eureka101v/WeiboSpiderGo](https://github.com/eureka101v/WeiboSpiderGo) A sina weibo(chinese twitter) scrapper
- [Go-phie/gophie](https://github.com/Go-phie/gophie) Search, Download and Stream movies from your terminal
- [imthaghost/goclone](https://github.com/imthaghost/goclone) Clone websites to your computer within seconds.
- [superiss/spidy](https://github.com/superiss/spidy) Crawl the web and collect expired domains.
- [docker-slim/docker-slim](https://github.com/docker-slim/docker-slim) Optimize your Docker containers to make them smaller and better.
- [seversky/gachifinder](https://github.com/seversky/gachifinder) an agent for asynchronous scraping, parsing and writing to some storages(elasticsearch for now)
- [eval-exec/goodreads](https://github.com/eval-exec/goodreads) crawl all tags and all pages of quotes from goodreads.

If you are using qcrawler in a project please send a pull request to add it to the list.

## Contributors

This project exists thanks to all the people who contribute. [[Contribute]](CONTRIBUTING.md).
<a href="https://gitlab.com/chedched/qcrawler/graphs/contributors"><img src="https://opencollective.com/qcrawler/contributors.svg?width=890" /></a>

## Backers

Thank you to all our backers! 🙏 [[Become a backer](https://opencollective.com/qcrawler#backer)]

<a href="https://opencollective.com/qcrawler#backers" target="_blank"><img src="https://opencollective.com/qcrawler/backers.svg?width=890"></a>

## Sponsors

Support this project by becoming a sponsor. Your logo will show up here with a link to your website. [[Become a sponsor](https://opencollective.com/qcrawler#sponsor)]

<a href="https://opencollective.com/qcrawler/sponsor/0/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/0/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/1/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/1/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/2/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/2/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/3/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/3/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/4/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/4/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/5/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/5/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/6/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/6/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/7/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/7/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/8/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/8/avatar.svg"></a>
<a href="https://opencollective.com/qcrawler/sponsor/9/website" target="_blank"><img src="https://opencollective.com/qcrawler/sponsor/9/avatar.svg"></a>

## License

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fgoqcrawler%2Fqcrawler.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fgoqcrawler%2Fqcrawler?ref=badge_large)
