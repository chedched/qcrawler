module gitlab.com/chedched/qcrawler

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/antchfx/htmlquery v1.2.4
	github.com/antchfx/xmlquery v1.3.9
	github.com/gobwas/glob v0.2.3
	github.com/jawher/mow.cli v1.2.0
	github.com/kennygrant/sanitize v1.2.4
	github.com/nlnwa/whatwg-url v0.1.0
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca
	github.com/temoto/robotstxt v1.1.2
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd
	google.golang.org/appengine v1.6.7
)
