package extensions

import (
	"gitlab.com/chedched/qcrawler"
)

// URLLengthFilter filters out requests with URLs longer than URLLengthLimit
func URLLengthFilter(c *qcrawler.Collector, URLLengthLimit int) {
	c.OnRequest(func(r *qcrawler.Request) {
		if len(r.URL.String()) > URLLengthLimit {
			r.Abort()
		}
	})
}
